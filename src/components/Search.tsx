import React from 'react';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { GlobalContext } from '../context/GlobalContext';
import useDebounce from '../utils/useDebounce';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      margin: 30,
      padding: 30,
    },
  })
);

export default function Search() {
  const globalContext = React.useContext(GlobalContext);
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = React.useState('');
  const [previousDebounced, setPreviousDebounced] = React.useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 500);
  React.useEffect(() => {
    if (debouncedSearchTerm) {
      if (debouncedSearchTerm !== previousDebounced) {
        setPreviousDebounced(debouncedSearchTerm);
        globalContext.getSearchedList(debouncedSearchTerm);
      }
    }
  }, [debouncedSearchTerm, globalContext, previousDebounced]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };
  const handleSelect = (event: any, newValue: string) => {
    globalContext.selectSymbol(newValue);
    setSearchTerm('');
  };

  return (
    <Paper className={classes.paper}>
      <Autocomplete
        freeSolo
        id='search'
        disableClearable
        options={globalContext.state.search.list.map(
          (option: any) => option['1. symbol']
        )}
        onChange={handleSelect}
        renderInput={(params) => (
          <TextField
            onChange={handleChange}
            value={searchTerm}
            {...params}
            label='Search symbol'
            margin='normal'
            variant='outlined'
            InputProps={{ ...params.InputProps, type: 'search' }}
          />
        )}
      />
    </Paper>
  );
}
