import React from 'react';
import Search from './components/Search';
import Graph from './components/Graph';
import LatestSymbols from './components/LatestSymbols';

function App() {
  return (
    <React.Fragment>
      <Search />
      <Graph />
      <LatestSymbols />
    </React.Fragment>
  );
}

export default App;
