import React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { GlobalContext } from '../context/GlobalContext';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      margin: 30,
      paddingLeft:30,
      paddingTop:30
    },
    button:{
      marginRight:30,
      marginBottom:30
    }
  })
);

export default function LatestSymbols() {
  const globalContext = React.useContext(GlobalContext);
  const classes = useStyles();

  const handleSelect=( symbol: string) => {
    globalContext.selectSymbol(symbol)
  }
  return (
    <Paper className={classes.paper}>
    {globalContext.state.latestSymbols.map((symbol:string)=>(
      <Button 
      variant="outlined" 
      size="large" 
      color="primary" 
      onClick={()=>handleSelect(symbol)}
      className={classes.button}
      >
        {symbol}
      </Button>
    ))}
    </Paper>
  );
}
