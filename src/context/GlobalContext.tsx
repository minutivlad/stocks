import * as React from 'react';

interface SearchedSymbol {
  title: string;
}

interface GraphData {
  interval: number;
  data: any[];
}

interface GlobalState {
  search: {
    list: SearchedSymbol[];
  };
  selectedSymbol: string;
  graph: GraphData;
  latestSymbols: string[];
}

const initialState: GlobalState = {
  search: {
    list: [],
  },
  selectedSymbol: '',
  graph: {
    interval: 1,
    data: [],
  },
  latestSymbols: [],
};

export const GlobalContext = React.createContext<{
  state: typeof initialState;
  getSearchedList: (searchTerm: string) => void;
  selectSymbol: (searchTerm: string) => void;
  changeInterval: (interval: number) => void;
}>({
  state: initialState,
  getSearchedList: () => {},
  selectSymbol: () => {},
  changeInterval: () => {},
});
export const GlobalConsumer = GlobalContext.Consumer;

export function GlobalProvider(props: any) {
  const [state, setState] = React.useState(initialState);
  React.useEffect(() => {
    if (state.selectedSymbol !== '') {
      getGraphData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.selectedSymbol, state.graph.interval]);

  const getSearchedList = (searchTerm: string) => {
    getApiData(
      `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${searchTerm}&apikey=`
    ).then((response: any) => {
      setState({
        ...state,
        search: {
          list: response.bestMatches,
        },
      });
    });
  };
  const selectSymbol = (symbol: string) => {
    const index = state.latestSymbols.indexOf(symbol);
    if (index === -1) {
      setState({
        ...state,
        selectedSymbol: symbol,
        latestSymbols: [symbol, ...state.latestSymbols],
      });
    } else {
      setState({
        ...state,
        selectedSymbol: symbol,
        latestSymbols: [
          symbol,
          ...state.latestSymbols.slice(0, index),
          ...state.latestSymbols.slice(index + 1),
        ],
      });
    }
  };

  const getGraphData = () => {
    getApiData(
      `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${state.selectedSymbol}&interval=${state.graph.interval}min&apikey=`
    ).then((response: any) => {
      setState({
        ...state,
        graph: {
          ...state.graph,
          data: Object.keys(
            response[`Time Series (${state.graph.interval}min)`]
          )
            .map((key: string) => {
              return {
                ...response[`Time Series (${state.graph.interval}min)`][key],
                time: key,
              };
            })
            .slice(0, 20),
        },
      });
    });
  };

  const changeInterval = (interval: number) =>
    setState({
      ...state,
      graph: {
        ...state.graph,
        interval,
      },
    });

  const value = { state, getSearchedList, selectSymbol, changeInterval };
  return (
    <GlobalContext.Provider value={value}>
      {props.children}
    </GlobalContext.Provider>
  );
}

function getApiData(url: string): any {
  const apiKey = 'ZVQ9F2W4940X2ZZC';
  return fetch(url + apiKey, {
    method: 'GET',
  })
    .then((r) => r.json())
    .then((r) => r)
    .catch((error) => {
      console.error(error);
      return [];
    });
}
