import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { GlobalContext } from '../context/GlobalContext';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {
  LineChart,
  Line,
  XAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      margin: 30,
      padding: 30,
    },
    paperHeader: {
      display: 'flex',
      flexDirection: 'row',
    },
    symbol: {
      marginRight: 30,
      width: 150,
    },
    formControl: {
      margin: -5,
      minWidth: 120,
    },
    chartContainer: {
      height: 500,
      width: '100%',
    },
  })
);

export default function Graph() {
  const globalContext = React.useContext(GlobalContext);
  const classes = useStyles();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    globalContext.changeInterval(event.target.value as number);
  };
  return (
    <Paper className={classes.paper}>
      <div className={classes.paperHeader}>
        <Typography variant='h4' component='h4' className={classes.symbol}>
          {globalContext.state.selectedSymbol}
        </Typography>
        <FormControl variant='outlined' className={classes.formControl}>
          <InputLabel id='interval-select-label'>Interval</InputLabel>
          <Select
            labelId='interval-select-label'
            id='interval-select'
            value={globalContext.state.graph.interval}
            onChange={handleChange}
            label='Interval'>
            <MenuItem value={1}>1 min</MenuItem>
            <MenuItem value={5}>5 min</MenuItem>
            <MenuItem value={15}>15 min</MenuItem>
            <MenuItem value={30}>30 min</MenuItem>
            <MenuItem value={60}>60 min</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={classes.chartContainer}>
        <ResponsiveContainer>
          <LineChart
            data={globalContext.state.graph.data}
            margin={{
              top: 20,
              right: 30,
              left: 20,
              bottom: 0,
            }}>
            <CartesianGrid strokeDasharray='3 3' />
            <XAxis dataKey='time' height={60} />
            <Tooltip />
            <Legend />
            <Line type='monotone' dataKey='1. open' stroke='#8884d8' />
            <Line type='monotone' dataKey='2. high' stroke='#2F8237' />
            <Line type='monotone' dataKey='3. low' stroke='#FF4410' />
            <Line type='monotone' dataKey='4. close' stroke='#3C2F7D' />
          </LineChart>
        </ResponsiveContainer>
      </div>
    </Paper>
  );
}
